#+TITLE: Meeting Minutes from streamlining the hosting process
#+AUTHOR: VLEAD
#+DATE: [2019-12-31 Tue]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document captures the minutes from the meeting to
streamline the hosting process of experiments. 

* Agenda
* Discussion
* Action Items
  1. Build a dashboard for vlead, from where one could look
     for processes, status of various projects, etc.
  2. Come up with an estimate for the effort in terms of
     full time engineers (FTE) for the central platform
     engineering to build hosting platform, realize
     services, devops.
  3. Come up with a cost estimate for AWS
