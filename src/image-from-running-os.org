* Creating a image from running OS
** Process: Booting from Live USB and de-attaching the file systems (5Hours)
- This process involves dd command with live usb
- Steps we have followed:
  - Insert the Linux Live USB and harddisk into your
    computer then poweroff the system.
  - Power on your system. Press "Enter" key immediately as
    Thinkpad logo appears to get into boot menu.
  - You should see a menu pop up with a list of choices on
    it. Press F12 for temporary startup disk Select your USB
    drive using arrow key and press Enter.
  - After system booting into OS. Open terminal and type
    below command to see the hard drives and mount points.
    #+BEGIN_EXAMPLE
    lbslk
    #+END_EXAMPLE
  - Make note of the harddrives that are shown on the
    desktop. The main hard drive has a name similiar to
    "/dev/sda." The secondary hard drive will has a name
    similar to "/dev/sdb."
  - We can identify them with their size too.( 512GB, 32GB,
    1TB)
  - Open Terminal and type the command "su" to switch to the root
    user.
  - Live USB's do not set up a root user, so we will not be
    prompted for a password.
  - -----
  - Type the below command to ensure that the disk you want
    to image is not mounted.
    #+BEGIN_EXAMPLE
    umount /dev/sda
    #+END_EXAMPLE   
  - Type the below command to mount the secondary disk
    drive(Harddisk). The first part of the command, /dev/sdb
    should be your secondary hardisk mount point. The second part of
    the command, "/mnt/sdb" is the directory where the disk
    will be accessed once it is mounted.
    #+BEGIN_EXAMPLE
    mount /dev/sdb /mnt/sdb
    #+END_EXAMPLE
  - Type the below command to create a
    compressed image of the original disk. Replace
    "/dev/sda" with the name of the original drive. Replace
    "/mnt/sdb" with the name of the directory.
    #+BEGIN_EXAMPLE
    dd if=/dev/sda conv=sync,noerror bs=64K | gzip -c > /mnt/sdb/laptop.img.gz
    #+END_EXAMPLE
  - -----
  - laptop.img.gz File is created(256GB) in span of 3
    hours. Copied to a hard disk drive and copied it to Mac
    then extracted it. Extraction took another 3 hours and
    gave a 256GB file.
  - Installed a VM in virual box then attached the VDI file
    to VM and enabled "Enable EFI" in the
    setting(settings>System>Motherboard)
  - Note: If full screen is not enabled > Go to top left of
    the screen > Find VM settings > Click on Devices option
    > Select "Insert Guest Additions CD Image"
- Refered Links:
  [[https://itstillworks.com/clone-hard-drive-ubuntu-6884403.html][https://itstillworks.com/clone-hard-drive-ubuntu-6884403.html]]
- Note : The above process takes about 5-6 hours.

** Process : Converting img file to VDI
  - Followed [[https://www.ostechnix.com/how-to-convert-img-file-to-vdi-file-using-oracle-virtualbox/][steps]] to convert IMG to VDI file.

- Refered Links: https://www.ostechnix.com/how-to-convert-img-file-to-vdi-file-using-oracle-virtualbox/ , https://www.silicongadget.com/operative-systems/virtualization/convert-img-files-virtualbox-vdi-format/2674/

