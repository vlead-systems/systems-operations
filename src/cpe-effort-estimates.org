#+TITLE: CPE man power estimates for Phase 4
#+AUTHOR: VLEAD
#+DATE: [2019-12-31 Tue]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document lists the manpower estimates for central
platform engineering (CPE) during the phase 4.


* CPE Agenda

CPE is responsible for hosting of experiments, ensure the
availability of the experiments, build services that are
consumed by the experiments, construct uniform UI and
provide operational support.

#+NAME: tbl-manpower-summary
#+CAPTION: Manpower Summary
|---+-------+----------------------------------+---------------|
|   | S.No. | Task                             | Manpower(FTE) |
|---+-------+----------------------------------+---------------|
| # |    1. | Uniform UI                       |             3 |
|---+-------+----------------------------------+---------------|
| # |    2. | New Kubernetes based cluster and |             2 |
|   |       | Integration with EKS             |               |
|---+-------+----------------------------------+---------------|
| # |    3. | Analytics                        |             1 |
|---+-------+----------------------------------+---------------|
| # |    4. | Performance & Security           |             1 |
|---+-------+----------------------------------+---------------|
| # |    5. | Operations                       |             1 |
|---+-------+----------------------------------+---------------|
|   |       | Total                            |             8 |
| ^ |       |                                  |         total |
|---+-------+----------------------------------+---------------|
#+TBLFM: $total=vsum(@II+1..@-1)
