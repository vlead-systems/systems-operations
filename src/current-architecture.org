#+TITLE: Current Architecture
#+AUTHOR: VLEAD
#+DATE: [2018-11-26 Mon]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


* Introduction
  This document details the current architecture of the
  Virtual Labs Hosting Platform.
* The Design
#+CAPTION: Different Clusters
#+NAME:   fig:vlead-cluster
#+CAPTION: 
  [[./img/cluster.png]]
* Different Machines
** Base Machines
*** base1 (10.4.12.21)
    This machine is set up as a testing area.  Virtual Labs
    cluster is set up on this machine.  It replicates the
    cluster on AWS.  The nodes in the cluster do not have
    public access.
    - OS :: CentOS release 6.7
    - Virtualization layer :: OpenVZ 2.6.32
    - Login :: key based

*** base2 (10.4.12.22)
    This machine is used for the cluster evolution.  This
    project right now is in abeyance.
    - OS :: Ubuntu 16.04.5 LTS
    - Login Credentials :: vlead/vlead123

*** base3 (10.4.12.23)
    This machine is set up with the STPI IP addresses.  The
    router, public dns ns1, ns2, private dns and reverse
    proxy are set up as containers on this machine.

    - PubliC IPS
      1. 196.12.53.133 
      2. 196.12.53.130 
      3. 196.12.53.134 
   
    - Configuration
      1. [[https://gitlab.com/vlead-systems/backup-config-files/blob/master/src/backup-config-files/stpi-router.vlabs.ac.in/sysconfig/iptables][Router]]
      2. [[https://gitlab.com/vlead-systems/backup-config-files/blob/master/src/backup-config-files/ns1-pub.vlabs.ac.in/var/named/virtual-labs.ac.in.forward][Public DNS 1 (ns1)]]
      3. [[https://gitlab.com/vlead-systems/backup-config-files/blob/master/src/backup-config-files/ns2-pub.vlabs.ac.in/var/named/slaves/virtual-labs.ac.in.forward][Public DNS 2 (ns2)]]
      4. [[https://gitlab.com/vlead-systems/backup-config-files/blob/master/src/backup-config-files/http.vlabs.ac.in/etc/httpd/conf.d/virtualhosts.conf][Reverse Proxy]]

    - OS :: CentOS release 6.7
    - Virtualization layer :: OpenVZ 2.6.32
    - Login :: LDAP

*** base4 (10.4.12.24)
    Access to this through LDAP.  Every with LDAP (non
    admin) access is allowed to create containers.  This is
    used by anyone at VLEAD to create a machine and test.
    - OS :: CentOS release 6.7
    - Virtualization layer :: OpenVZ 2.6.32
    - Login :: LDAP

** AWS
   The cluster on AWS contains multiple EC2 virtual machines
   with couple of machines (config server and router) set up
   with public IP.  All the machines that make up the
   cluster reside in a subnet.

*** Configurations
   1. [[https://gitlab.com/vlead-systems/systems-model/blob/master/src/router.org][Router]]
   2. [[https://gitlab.com/vlead-systems/systems-model/blob/master/src/public-dns.org][Public DNS]]
   3. [[https://gitlab.com/vlead-systems/systems-model/blob/master/src/rp-awstats.org][Reverse Proxy]]

* Access to AWS 
  Right now VLEAD access the machines on AWS via the
  ssh-tunnel.vlabs.ac.in.  This is configured with an STPI
  IP.  Once the STPI service is revoked, IIIT has to provide
  access to AWS for the VLEAD. 
* Check access to Pascal Machine
  Right now, pascal.iiit.ac.in is hosted on one of the
  containers on base3 machine with the IP =10.4.12.165=.
  This machine should still be accessible when STPI link is
  revoked.
* Two more machines to be migrated
  dev.vlabs.ac.in and exp.vlabs.ac.in are containers on
  base3 and base4 machines.  The primary DNS on AWS is
  specified to resolve these domain names to the
  =192.12.53.130=.  The NAT rule routes the 443/80 traffic
  to a reverse proxy, and the reverse proxy then routes the
  traffic to appropriate services.

  Therefore, these machines should be be moved to AWS when
  STPI link is revoked.
