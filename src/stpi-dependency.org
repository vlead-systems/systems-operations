#+TITLE: 
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This is an effort to remove the dependency of STPI on the
  VLEAD cluster.

* Steps
** Email to Ernet
1. Sent an email to ernet to change the mapping of IP to
   ns.vlabs.ac.in

** Access to Ansible
   1. Stopped and diabled all the linux firewall rules
   #+BEGIN_EXAMPLE
   service iptables stop
   chkconfig iptables off
   #+END_EXAMPLE

   2. Enabled two factor authentication with both a key and
      a password.  This is done by adding the following
      lines to the file =/etc/ssh/sshd_config= on the
      ansible machine.

      #+BEGIN_EXAMPLE
      RequiredAuthentications2 publickey,password
      PermitEmptyPasswords no
      PasswordAuthentication yes
      #+END_EXAMPLE

* Notes with Shiva
  1. Check whether internet is there?
     #+BEGIN_EXAMPLE
     route -n
     #+END_EXAMPLE
     
     gives the following output

     #+BEGIN_EXAMPLE
     Kernel IP routing table
     Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
     0.0.0.0         10.100.1.1      0.0.0.0         UG    0      0        0 eth0
     10.100.0.0      0.0.0.0         255.255.252.0   U     0      0        0 eth0
     #+END_EXAMPLE

     If gateway is not set to router's private IP
     (10.100.1.1), then first add and then delete.
     
     #+BEGIN_EXAMPLE
     add default gw 10.100.1.1 ;;this is router's private IP
     del default gw 10.100.0.1  
     #+END_EXAMPLE

  2. commands
     #+BEGIN_EXAMPLE
     vim roles/common_vars/vars/main.yml
     ansible-playbook -i hosts reverseproxy_server.yaml 
     ansible-playbook -i hosts public_dns.yaml
     ansible-playbook -i hosts private_dns.yaml 
     ansible-playbook -i hosts router.yaml 
     ansible-playbook -i hosts reverseproxy_server.yaml 
     #+END_EXAMPLE




     
